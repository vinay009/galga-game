
import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let screenSize = UIScreen.main.bounds.size
    var player = SKSpriteNode()
    
    // keep track of all the Node objects on the screen
    var bullets : [SKSpriteNode] = []
    var enemies : [SKSpriteNode] = []
    var timeLabel: SKLabelNode!
    let livesLabel = SKLabelNode(text: "Lives: ")
    let scoreLabel = SKLabelNode(text: "Score: ")
    
    // GAME STATISTIC VARIABLES
    var lives = 3           // for testing, use a small number
    var score = 0
    //timer
    var timeLeft = 119
    
    let bulletSound = SKAction.playSoundFileNamed("bulletsound.wav", waitForCompletion: false)
    var timer = Timer()
    override func didMove(to view: SKView) {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(GameScene.updateTime), userInfo: nil, repeats: true)
        let background = SKSpriteNode(imageNamed: "background.png")
        background.size = self.size
        background.position  = CGPoint(x:self.size.width/2 , y:self.size.height/2)
        background.zPosition = -1
        
        let Bomb = SKSpriteNode(imageNamed: "bomb")
        Bomb.setScale(0.4)
        Bomb.position = CGPoint(x:self.size.width-50 , y:100)
        
        
        self.addChild(background)
        self.addChild(Bomb)
        
        // MARK: Add a lives label
        // ------------------------
        self.livesLabel.text = "Lives: \(self.lives)"
        self.livesLabel.fontName = "Avenir-Bold"
        self.livesLabel.fontColor = UIColor.magenta
        self.livesLabel.fontSize = 25;
        self.livesLabel.position = CGPoint(x:self.size.width-50,
                                           y:700)
        
        
        // MARK: Add a score label
        // ------------------------
        self.scoreLabel.text = "Score: \(self.score)"
        self.scoreLabel.fontName = "Avenir-Bold"
        self.scoreLabel.fontColor = UIColor.magenta
        self.scoreLabel.fontSize = 25;
        self.scoreLabel.position = CGPoint(x:self.size.width-350,
                                           y:700)
        
        // Label for time
        timeLabel = SKLabelNode(fontNamed: "Avenir-Bold")
        timeLabel.fontSize = 22
        timeLabel.fontColor = UIColor.red
        timeLabel.text = "Time Left: 2:00"
        timeLabel.horizontalAlignmentMode = .right
        timeLabel.verticalAlignmentMode = .top
        timeLabel.position = CGPoint(x: self.size.width-120, y:720)
        
        
        // MARK: Add your sprites to the screen
        
        addChild(self.livesLabel)
        addChild(self.scoreLabel)
        addChild(timeLabel)
        
        
        self.setupSpaceShip()
        self.setupEnemies()
        
        
    }
    
    // variable to keep track of how much time has passed
    var timeOfLastUpdate:TimeInterval?
    
    override func update(_ currentTime: TimeInterval) {
        
        if (timeOfLastUpdate == nil) {
            timeOfLastUpdate = currentTime
        }
        
        let timePassed = (currentTime - timeOfLastUpdate!)
        if (timePassed >= 1) {
            timeOfLastUpdate = currentTime
            // create a bullet
            self.createBullet()
            self.moveEnemy()
        }
        
        
        self.detectCollision()
        
    }
    //bullet from the player
    func fireBullet() {
        let bullet = SKSpriteNode(imageNamed: "bullet2")
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody?.affectedByGravity = false
        bullet.setScale(0.5)
        bullet.position = player.position
        bullet.zPosition = 0
        bullet.position.y += 70
        self.addChild(bullet)
        
        let moveBullet = SKAction.moveTo(y: self.size.height + bullet.size.height, duration: 1)
        let deleteBullet = SKAction.removeFromParent()
        let bulletSequence = SKAction.sequence([bulletSound,moveBullet,deleteBullet])
        bullet.run(bulletSequence)
    }
    
    
    var mousePosition:CGPoint?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        mousePosition = touches.first?.location(in: self)
        
        //PLspeed = 20
        
        let center = self.size.width / 2
        if(mousePosition!.x < center) {
            // MOve Left
            if(self.player.position.x >= 0) {
                self.player.position.x -= 40
            }
        } else if (mousePosition!.x > center) {
            // Move right
            if((self.player.position.x) <= (self.size.width - self.player.size.width)) {
                self.player.position.x += 40
            }
        }
        
        
        
        // 1. Dectect what sprit was touched
        let spriteTouched = self.atPoint(mousePosition!)
        
        // 2. check if he touched tree
        if(spriteTouched.name == "Bomb")
        {
            self.fireBullet()
        }
    }
    
    
    
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    var minutes = 0
    var seconds = 59
    @objc func updateTime() {
        
        if(timeLeft >= 0){
            minutes = (timeLeft / 60)
            seconds = (timeLeft % 60)
            if(seconds <= 9){
                timeLabel.text = "Time Left: " + String(minutes) + ":0" + String(seconds)
            } else {
                timeLabel.text = "Time Left: " + String(minutes) + ":" + String(seconds)
            }
            timeLeft -= 1
        }
        
        if(timeLeft <= -1) {
            timer.invalidate()
        }
        
    }
    
    func setupSpaceShip(){
        player = SKSpriteNode.init(texture: SKTexture(imageNamed: "player"))
        player.size = CGSize.init(width: 50, height: 50)
        player.position = CGPoint.init(x: (screenSize.width/2), y: 80)
        addChild(player)
    }
    //spawning the enemies
    func setupEnemies(){
        
        let eCount : Int = 15
        let space : CGFloat = 30.0
        let height : CGFloat = 40.0
        let width : CGFloat = 40.0
        
        var xPosition : CGFloat = width + space
        var yPosition : CGFloat = screenSize.height - (height + space)
        
        
        for eIndex in 0...eCount {
            print(eIndex)
            
            let enemy = SKSpriteNode.init(texture: SKTexture(imageNamed: self.getEnemyImageName()))
            enemy.size = CGSize.init(width: width, height: height)
            enemy.zPosition = 1
            enemy.position = CGPoint.init(x: xPosition, y: yPosition)
            addChild(enemy)
            enemies.append(enemy)
            
            xPosition += space + width
            if xPosition >= screenSize.width - space{
                yPosition -= height + space
                xPosition = width + space
            }
        }
    }
    
    func getEnemyImageName() -> String{
        let enemyImageIndex = Int(CGFloat(arc4random_uniform(UInt32(3))))
        
        switch enemyImageIndex {
        case 0:
            return "enemy1"
        case 1:
            return "enemy2"
        case 2:
            return "enemy3"
        default:
            return "enemy1"
        }
        
    }
    
    //spawning the bullets and removing from the screen
    func createBullet() {
        
        let bullet = SKSpriteNode(imageNamed: "bullet")
        bullet.setScale(0.3)
        bullet.position = player.position
        bullet.zPosition = -1
        bullet.position.y += 20
        self.addChild(bullet)
        let moveBullet = SKAction.moveTo(y: self.size.height + bullet.size.height, duration: 1)
        let deleteBullet = SKAction.removeFromParent()
        let bulletSequence = SKAction.sequence([bulletSound,moveBullet,deleteBullet])
        bullet.run(bulletSequence)
        
        self.bullets.append(bullet)
        
        
        
        
    }
    
    func fireBullet(bullet: SKNode, toDestination destination: CGPoint, withDuration duration: CFTimeInterval, andSoundFileName soundName: String) {
        let bulletAction = SKAction.sequence([SKAction.move(to: destination, duration: duration)])
        //, SKAction.wait(forDuration: 3.0 / 60.0),SKAction.removeFromParent()
        //let soundAction = SKAction.playSoundFileNamed(soundName, waitForCompletion: true)
        bullet.run(SKAction.group([bulletAction]))
        
        // add the bullet to the scene
        addChild(bullet)
    }
    
    func moveEnemy(){
        if self.enemies.count > 0{
            let enemyIndex = Int(CGFloat(arc4random_uniform(UInt32(self.enemies.count))))
            let enemyDestination = CGPoint(x:player.position.x , y:0.0)
            let enemyAction = SKAction.move(to: enemyDestination, duration: 15.0)
            self.enemies[enemyIndex].run(enemyAction)
            //bullet.position.y += 70
        }
    }
    
    
    
    func detectCollision(){
        
        for (enemyIndex, enemy) in enemies.enumerated() {
            
            for (arrayIndex, bullet) in bullets.enumerated() {
                
                if bullet.intersects(enemy){
                    
                    self.score = self.score + 10
                    self.scoreLabel.text = "Score: \(self.score)"
                    enemy.removeFromParent()
                    self.enemies.remove(at: enemyIndex)
                    
                    bullet.removeFromParent()
                    self.bullets.remove(at: arrayIndex)
                    
                    if (self.enemies.count == 0 || timeLeft <= 0) {
                        // YOU WIN!!!!
                        let winScene = WinScene(size: self.size)
                        
                        // CONFIGURE THE LOSE SCENE
                        winScene.scaleMode = .aspectFill
                        
                        // MAKE AN ANIMATION SWAPPING TO THE LOSE SCENE
                        let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
                        self.view?.presentScene(winScene, transition: transitionEffect)
                        
                        
                        break;
                    }
                    
                }
                
                if (bullet.position.y >= self.size.height)  {
                    //top of screen
                    bullet.removeFromParent()
                    self.bullets.remove(at: arrayIndex)
                }
                
            }
            
            
            if enemy.intersects(player){
                enemy.removeFromParent()
                self.enemies.remove(at: enemyIndex)
                self.lives = self.lives - 1
                // 2. update the lives label
                self.livesLabel.text = "Lives: \(self.lives)"
                
                if (self.lives == 0) {
                    // DISPLAY THE YOU LOSE SCENE
                    let loseScene = GameOverScene(size: self.size)
                    
                    // CONFIGURE THE LOSE SCENE
                    loseScene.scaleMode = .aspectFill
                    
                    // MAKE AN ANIMATION SWAPPING TO THE LOSE SCENE
                    let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
                    self.view?.presentScene(loseScene, transition: transitionEffect)
                    
                }
            }
            
            if (enemy.position.y <= 0.0)  {
                //Bottom of screen
                enemy.removeFromParent()
                self.enemies.remove(at: enemyIndex)
            }
            
        }
        
        
    }
}
